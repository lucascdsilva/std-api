<?php
namespace StdAPI\Http\Router;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use StdAPI\Http\Router;

class Setup 
{
	private $reader;
	private $controllers;

	public function __construct($loader, $controllers=array()) 
	{
		AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
		$this->reader = new AnnotationReader();
		$this->controllers = $controllers;
	}

	public function run(Router $router, $di) 
	{
		$allowMethods = ['GET', 'POST', 'PUT', 'DELETE'];

		foreach ($this->controllers as $key => $ctrl) {
			if (!class_exists($ctrl))
				throw new \Exception(sprintf("Not Found Controller File %s", $ctrl), 1);
			
			$methods = get_class_methods($ctrl);
			foreach ($methods as $key => $method) {
				$reflMethod = new \ReflectionMethod($ctrl, $method);
				$methodAnnotations = $this->reader->getMethodAnnotations($reflMethod);
				foreach ($methodAnnotations AS $route) {
					if (in_array($route->method, $allowMethods)) {
						$methodCall = strtolower($route->method);
						$router->$methodCall($route->uri, array($ctrl, $method, $di), $route->allow);
					}
				}
			}
		}
		return $router;
	}
}