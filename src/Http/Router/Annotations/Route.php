<?php
namespace StdAPI\Http\Router\Annotations;

/**
* @Annotation
* @Target({"METHOD"})
*/
class Route
{
	public $method;
	public $allow;
	public $uri;
	
	public function __construct($properties)
	{
		$this->method = $properties['method'];
		$this->uri    = $properties['uri'];
		$this->allow  = $properties['allow'];
	}
}