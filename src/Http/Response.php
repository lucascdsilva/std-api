<?php
namespace StdAPI\Http;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response extends SymfonyResponse 
{
	private $data;
	private $type='json';

	public function redirect($url, $code=302) 
	{
		header(sprintf("Location: %s", $url), TRUE, $code);
	}

	public function render($view)
	{
		http_response_code(200);
		echo file_get_contents($view);
		exit;
	}
	
	public function response($data, $code=200) 
	{
		$this->data = $data;
		$this->code = $code;
		
		http_response_code($this->code);
		if ($this->type=='json') {
			$this->response_json();			
		} else {
			throw new Exception("Type Response not implements", 1);
		}
	}

	private function response_json() 
	{
		$this->headers->set('Content-Type', 'application/json;charset=utf-8');
		$this->setContent(json_encode($this->data));
		$this->setStatusCode($this->code);
		return $this->send();
	}
}