<?php
namespace StdAPI\Http;
use StdAPI\Authentication\Auth;

class Router
{
	private $found_route = false;
	private $allowAcess = false;
	private $satisfied = false;
	private $token = false;
	private $roles = array();
	private $baseUrl = '';
	private $request;
	private $response;
	private $uri;

	public function __construct(Request $request, Auth $auth, Response $response) 
	{
		$this->token = $auth->getToken($request->getAuthorization());
		$request->setToken($this->token);
		$this->roles = $auth->getRoles();
		$this->request = $request;
		$this->response = $response;
		$this->uri = preg_replace('/[\?|#](.*)/', '', $request->getRequestUri());
	}

	public function getToken() 
	{
		return $this->token;
	}

	public function get($name, $call, $req_roles=[]) 
	{
		$this->find_route('GET', $name, $call, $req_roles);
	}

	public function post($name, $call, $req_roles=[]) 
	{
		$this->find_route('POST', $name, $call, $req_roles);
	}

	public function put($name, $call, $req_roles=[]) 
	{
		$this->find_route('PUT', $name, $call, $req_roles);
	}

	public function delete($name, $call, $req_roles=[]) 
	{
		$this->find_route('DELETE', $name, $call, $req_roles);
	}

	public function options($name, $call, $req_roles=[]) 
	{
		$this->find_route('OPTIONS', $name, $call, $req_roles);
	}
	
	public function setBaseUrl($base)
	{
		$this->baseUrl = $base;
	}

	public function notFound($call) 
	{
		if (!$this->found_route && is_callable($call)) $call($this->request, $this->response);
	}

	public function forbidden($call)
	{
		if ($this->found_route && !$this->allowAcess && is_callable($call)) 
			$call($this->request, $this->response);
	}

	private function preg_route($route, &$data=[])
	{
		$regex = preg_replace('/\$([a-z|_]*):/', '', $route);
		$regex = sprintf('/^%s$/', str_replace('/', '\/', $regex));

		$values = [];
		
		if (preg_match($regex, $this->uri, $values)) {
			$keys = [];
			preg_match_all('/\$([a-z|_]*):/', $route, $keys);
			array_shift($values);
			array_shift($keys);
			foreach ($keys[0] as $key => $value) {	
				if (isset($values[$key])) {
					$data[$value] = $values[$key];
				}
			}
			return true;
		}
		return false;
	}

	private function find_route($method, $name, $call, $req_roles) 
	{
		if($this->found_route && $this->allowAcess) return;
		$route = $this->baseUrl.$name;
		$matches = [];

		if ($this->request->getMethod() == $method && $this->preg_route($route, $matches)) {
			$this->call_route($name, $call, $req_roles, $matches);
			$this->found_route = true;
		}
	}

	private function action_method($signature, $response, $matches)
	{
		$class = $signature[0];
		$method = $signature[1];
		$dependencyInjection = $signature[2];

		$controller = new $class($dependencyInjection);
		if (empty($matches)) {
			return $controller->$method($this->request, $response);
		}
		return $controller->$method($this->request, $response, $matches);
	}

	private function call_route($name, $call, $req_roles, $matches) 
	{
		if (empty($req_roles) || $this->allow_access($req_roles)) {
			$this->allowAcess = true;
			if (is_callable($call)) {
				if (!empty($matches)) {
					$call($this->request, $this->response, $matches);
					return;
				}
				$call($this->request, $this->response);
				return;
			}
			
			if (is_array($call)) {

				if (!is_callable($call[0]) && class_exists($call[0]) && count($call) >= 3) {
					$this->action_method($call, $this->response, $matches);
					return;
				}

				if (!empty($matches)) {
					$call[0]->$call[1]($this->request, $this->response);
					return;
				}
				$call[0]->$call[1]($this->request, $this->response, $matches);
				return;
			}
		}
	}

	private function allow_access($req_roles) 
	{
		foreach ($this->roles as $key => $item) {
			if (in_array($item, $req_roles)) return true;
		}
		return false;
	}
}

