<?php
namespace StdAPI\Http;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class Request extends SymfonyRequest
{
	private $authorization = false;
	private $token = null;

	public function setToken($token)
	{
		$this->token = $token;
	}

	public function getToken()
	{
		return $this->token;
	}
	
	public function getAuthorization()
	{
		$this->setAuthorization();
		return $this->authorization;
	}

	private function setAuthorization()
	{
		if ($this->headers->get('Authorization')) {
			$this->authorization = $this->headers->get('Authorization');
		} else if($this->getSession() && $this->getSession()->get('Authorization')) {
			$this->authorization = $this->getSession()->get('Authorization');
		} else if($this->query->get('token')) {
			$this->authorization = $this->query->get('token');
		}
	}
}