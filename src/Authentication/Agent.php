<?php
namespace StdAPI\Authentication;

interface Agent 
{
	public function getId();
	public function getAgentRoles();
}