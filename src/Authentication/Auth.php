<?php
namespace StdAPI\Authentication;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class Auth
{
    private $token = false;
    private $expiration;
    public function __construct(int $expiration=null) 
    {
        $this->expiration = $expiration
        ? $expiration
        : 3600;
    }

	public function generate_token(Agent $agent)
	{
        $token = (new Builder())->setIssuer(JWT_ISS) // Configures the issuer (iss claim)
        ->setAudience(JWT_AUD) // Configures the audience (aud claim)
        ->setId(JWT_JIT, true) // Configures the id (jti claim), replicating as a header item
        ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
        ->setNotBefore(time()) // Configures the time that the token can be used (nbf claim)
        ->setExpiration(time() + $this->expiration) // Configures the expiration time of the token (exp claim)
        ->set('uid', $agent->getId()) // Configures a new claim, called "uid"
        ->set('roles', $agent->getAgentRoles()) // Configures a new claim, called "uid"
        ->getToken(); // Retrieves the generated token
        return (string) $token;
    }

    public function getRoles()
    {
        $roles = $this->authenticate($this->token);
        return (is_array($roles)) ? $roles : array();
    }

    public function valid($authorization_token)
    {
        try {
            $token = (new Parser())->parse((string) $authorization_token);
            $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
            $data->setIssuer(JWT_ISS);
            $data->setAudience(JWT_AUD);
            $data->setId(JWT_JIT);
            if($token->validate($data)) {
                return $token;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getToken($authorization)
    {
        if (!$authorization) return false;
         try {
            return $this->token = $this->valid($authorization);
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }

    private function authenticate($token)
    {
        if (!$token) return false;
        try {
            $token = (new Parser())->parse((string) $token);
            $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
            $data->setIssuer(JWT_ISS);
            $data->setAudience(JWT_AUD);
            $data->setId(JWT_JIT);
    
            if ($token->validate($data)) {
                return  $token->getClaim('roles');
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}